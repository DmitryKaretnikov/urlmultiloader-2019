This application is for searching the text at HTML pages.

The search starts after the user entered the input parameters (Start URL, Maximum number of threads, Search text, Maximum number of scanned URL) and pressed the Start button. If the input parameters were correct, the application will start searching - the application will check for the presence of the search text and a link for further search. During the search, the application updates the list of pages and the progress bar. Updating the list to the current state is limited to once per second. The page is considered as HTML if Content-Type: text / HTML is present or if the first line contains <! DOCTYPE html>. When parsing a page, links are considered by regular expression: https?: // (www.)? [-A-zA-Z0-9 @:%. + ~ # =] {2,256}. [Az] {2,4} \ b ([- a-zA-Z0-9 @:% +. ~ #? & / =] *)

Each of the pages in the list may have the following state:

Gray - the link has not been processed yet  
Orange - the link is in processing  
Green - the text to be found was founded on the link page   
Red - an error occurred during the operation. The user can click on this link and see a dialog with information about the error message  
Black - the searched text was not found  

User can control the process using the following commands:

Start button - stop the previous search, clear the list and starts a new search.  
Stop button - stop the search, but the user can still see its results.   
Pause switch - pauses the search, after disabling of pause - the search continues from the first links that have not been processed. Also, the pause mode will not start a new search  

Also, the application contains examples of Unit and integration tests.

Possible TODOs: Separate the list and controls into separate fragments (too many states get into the render), optimize the flow of a new task, process edge cases

The application artifact storage in the Downloads tab in this repository.

P.S. Anko Layouts was used only because it has not been used for a long time) Android Studio 3.4 has lost support for the plug-in that displays the preview of the layout, which make difficult to use this library