package com.dakarkaret.developexapp

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.dakarkaret.developexapp.feature.main.MainActivity
import com.dakarkaret.developexapp.feature.main.MainLayout
import org.junit.After
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainScreenUITests {

    @Rule
    @JvmField
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java, false, false
    )

    /**
     * Given - Starting state
     * When - Enter input values into all [EditText]s and press Start btn
     * Then - List of loaded urls showed, wait for finish first url, and check it
     */
    @Test
    fun successTest() {
        activityRule.launchActivity(null)

        viewWithId(MainLayout.PAUSE_SWITCH_ID).isVisible()

        viewWithId(MainLayout.SEARCH_START_URL_EDIT_ID).type("http://www.columbia.edu/~fdc/sample.html")
        viewWithId(MainLayout.SEARCH_TEXT_EDIT_ID).type("commands")
        viewWithId(MainLayout.SEARCH_THREADS_EDIT_ID).type("4")
        viewWithId(MainLayout.SEARCH_LIMIT_EDIT_ID).type("50")

        viewWithId(MainLayout.START_BUTTON_ID).click()

        val resultAdapter = activityRule.activity.findViewById<RecyclerView>(MainLayout.RECYCLER_ID).adapter
        waitForCondition(Instruction("Wait for first nodes upload") { resultAdapter?.run { itemCount > 1 } ?: false })

        viewWithId(MainLayout.RECYCLER_ID).hasViewWithTextAtPosition(0, "http://www.columbia.edu/~fdc/sample.html")
    }

    @After
    fun after() {
        activityRule.finishActivity()
    }

    private fun viewWithId(id: Int) = Espresso.onView(ViewMatchers.withId(id))!!

    private fun ViewInteraction.click() = this.perform(ViewActions.click())!!

    private fun ViewInteraction.type(text: String) =
        this.perform(ViewActions.clearText()).perform(ViewActions.typeText(text))!!

    private fun ViewInteraction.isVisible() = this.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))!!

    private fun ViewInteraction.hasViewWithTextAtPosition(index: Int, text: CharSequence): ViewInteraction =
        this.check { view, e ->
            if (view !is RecyclerView) {
                throw e
            }
            val outViews = ArrayList<View>()
            view.findViewHolderForAdapterPosition(index)?.itemView?.findViewsWithText(
                outViews, text, View.FIND_VIEWS_WITH_TEXT
            )
            Assert.assertTrue(
                "There's no view at index $index of recyclerView that has text : $text", outViews.isNotEmpty()
            )
        }

}