package com.dakarkaret.developexapp

import java.util.concurrent.TimeUnit

private const val CONDITION_NOT_MET = 0
private const val CONDITION_MET = 1
private const val TIMEOUT = 2

@Throws(Exception::class)
fun waitForCondition(
    instruction: Instruction, delayWhenConditionIsMetMs: Long = 1,
    timeoutLimit: Long = TimeUnit.SECONDS.toMillis(60), watchInterval: Int = 250
) {
    var status = CONDITION_NOT_MET
    var elapsedTime = 0

    do {
        if (instruction.checkCondition()) {
            status = CONDITION_MET
        } else {
            elapsedTime += watchInterval
            Thread.sleep(watchInterval.toLong())
        }

        if (elapsedTime >= timeoutLimit) {
            status = TIMEOUT
            break
        }
    } while (status != CONDITION_MET)

    Thread.sleep(delayWhenConditionIsMetMs)

    if (status == TIMEOUT)
        throw Exception("${instruction.description} - took more than ${TimeUnit.MILLISECONDS.toSeconds(timeoutLimit)} seconds. Test stopped.")
}

open class Instruction(val description: String, val checkCondition: () -> Boolean)