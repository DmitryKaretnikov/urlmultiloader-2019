package com.dakarkaret.developexapp

import android.app.Application
import com.dakarkaret.developexapp.di.featureModule
import com.dakarkaret.developexapp.di.reposModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        startKoin {
            androidContext(this@App)
            modules(listOf(featureModule, reposModule))
        }
        super.onCreate()
    }
}