package com.dakarkaret.developexapp.di

import com.dakarkaret.developexapp.feature.main.MainActivity
import com.dakarkaret.developexapp.feature.main.MainLayout
import com.dakarkaret.developexapp.feature.main.MainPresenter
import com.dakarkaret.developexapp.feature.main.search.SearchAdapter
import org.koin.core.qualifier.named
import org.koin.dsl.module


val featureModule = module {

    scope(named<MainActivity>()) {

        scoped { MainPresenter(get()) }

        scoped { MainLayout(get()) }

        scoped { SearchAdapter() }
    }
}
