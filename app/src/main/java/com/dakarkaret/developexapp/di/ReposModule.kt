package com.dakarkaret.developexapp.di

import com.dakarkaret.developexapp.repo.html.HtmlRepo
import com.dakarkaret.developexapp.repo.html.HtmlRepoImpl
import com.dakarkaret.developexapp.repo.nodeLoader.UrlNodeGraph
import com.dakarkaret.developexapp.repo.nodeLoader.UrlNodeGraphImpl
import com.dakarkaret.developexapp.repo.remote.UrlRepo
import com.dakarkaret.developexapp.repo.remote.UrlRepoImpl
import org.koin.dsl.module


val reposModule = module {

    single<HtmlRepo> { HtmlRepoImpl() }

    single<UrlNodeGraph> { UrlNodeGraphImpl(get()) }

    single<UrlRepo> { UrlRepoImpl(get()) }
}