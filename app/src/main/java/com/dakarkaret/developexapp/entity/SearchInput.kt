package com.dakarkaret.developexapp.entity

data class SearchInput(
    val startUrl: String,
    val searchText: String,
    val threads: Int,
    val searchLimit: Int
) {

    fun isCorrect() = startUrl.isNotEmpty() && searchText.isNotEmpty() && threads > 0 && searchLimit > 0
}