package com.dakarkaret.developexapp.entity

data class UrlNode(
    val url: String,
    var status: UrlStatus = UrlStatus.WAIT,
    var error: Exception? = null
) {

    val children: MutableList<UrlNode> = mutableListOf()

    fun addChild(node: UrlNode) {
        children.add(node)
    }
}

enum class UrlStatus(val color: Int) {
    WAIT(android.R.color.darker_gray),
    LOADING(android.R.color.holo_orange_dark),
    SUCCESS(android.R.color.holo_green_dark),
    EMPTY(android.R.color.black),
    ERROR(android.R.color.holo_red_dark)
}