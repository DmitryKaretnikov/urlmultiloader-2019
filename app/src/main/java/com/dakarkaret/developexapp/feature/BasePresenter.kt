package com.dakarkaret.developexapp.feature

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.core.KoinComponent

abstract class BasePresenter<V : MvpView?, VS> : MviBasePresenter<V, VS>(), KoinComponent {

    protected val disposables: CompositeDisposable = CompositeDisposable()

    override fun destroy() {
        disposables.clear()
        super.destroy()
    }

    protected fun Disposable.disposeOnDestroy() {
        disposables.add(this)
    }
}