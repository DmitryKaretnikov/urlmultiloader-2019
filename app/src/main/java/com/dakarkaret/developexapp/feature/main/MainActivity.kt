package com.dakarkaret.developexapp.feature.main

import android.os.Bundle
import android.util.Log.d
import android.util.Log.e
import com.dakarkaret.developexapp.entity.SearchInput
import com.dakarkaret.developexapp.feature.main.search.SearchAdapter
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.alert
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast
import org.koin.androidx.scope.currentScope
import org.koin.core.KoinComponent

class MainActivity : MviActivity<MainView, MainPresenter>(), MainView, KoinComponent {

    private val mainLayout: MainLayout by currentScope.inject()
    private val searchAdapter: SearchAdapter by currentScope.inject()
    private val hideAlertSubject = PublishSubject.create<Unit>().toSerialized()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainLayout.setContentView(this)
    }

    override fun createPresenter(): MainPresenter = currentScope.get()

    override fun render(viewState: MainViewState) {
        viewState.run {
            d("MainViewImpl", "ViewState: $viewState")

            mainLayout.updateProgress(progress)
            searchAdapter.searches = urls
            mainLayout.updatePauseSwitch(paused)

            when (this) {
                is Error -> {
                    e("MainViewImpl", "Error: ", error)
                    toast("${error.message}").show()
                }
                is ErrorDialog -> {
                    alert {
                        message = "${error.message}"
                        onCancelled { hideAlertSubject.onNext(Unit) }
                    }.show()
                }
                else -> {
                }
            }
        }
    }

    override fun startIntent(): Observable<SearchInput> = RxView.clicks(mainLayout.startBtn)
        .map { mainLayout.getSearchInput() }

    override fun stopIntent(): Observable<Unit> = RxView.clicks(mainLayout.stopBtn).map { Unit }

    override fun pauseIntent(): Observable<Boolean> = RxCompoundButton.checkedChanges(mainLayout.pauseSwitch)

    override fun searchExceptionIntent(): Observable<Throwable> = searchAdapter.getSearchThrowableObservable()

    override fun hideDialogIntent(): Observable<Unit> = hideAlertSubject
}
