package com.dakarkaret.developexapp.feature.main

import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Switch
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.recyclerview.widget.LinearLayoutManager
import com.dakarkaret.developexapp.R
import com.dakarkaret.developexapp.entity.SearchInput
import com.dakarkaret.developexapp.feature.main.search.SearchAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.*
import org.jetbrains.anko.constraint.layout._ConstraintLayout
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainLayout(private val searchAdapter: SearchAdapter) : AnkoComponent<MainActivity> {

    companion object {
        const val PROGRESS_ID = 1001
        const val START_BUTTON_ID = 1002
        const val STOP_BUTTON_ID = 1003
        const val PAUSE_SWITCH_ID = 1004
        const val RECYCLER_ID = 1005
        const val SEARCH_START_URL_EDIT_ID = 1006
        const val SEARCH_TEXT_EDIT_ID = 1007
        const val SEARCH_THREADS_EDIT_ID = 1008
        const val SEARCH_LIMIT_EDIT_ID = 1009
    }

    private lateinit var progressBar: ProgressBar
    lateinit var startBtn: Button
        private set
    lateinit var stopBtn: Button
        private set
    lateinit var pauseSwitch: Switch
        private set
    private lateinit var startUrlField: EditText
    private lateinit var searchTextField: EditText
    private lateinit var threadsField: EditText
    private lateinit var searchLimitField: EditText

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui) {
        constraintLayout {
            progressBar = horizontalProgressBar {
                id = PROGRESS_ID
            }.lparams(matchConstraint, wrapContent)

            startBtn = button {
                id = START_BUTTON_ID
                text = context.getString(R.string.start)
            }.lparams(matchConstraint, wrapContent)

            stopBtn = button {
                id = STOP_BUTTON_ID
                text = context.getString(R.string.stop)
            }.lparams(matchConstraint, wrapContent)

            pauseSwitch = switch {
                id = PAUSE_SWITCH_ID
                text = context.getString(R.string.paused)
                isChecked = false
            }.lparams(matchConstraint, wrapContent)

            startUrlField = editText {
                id = SEARCH_START_URL_EDIT_ID
                hint = context.getString(R.string.search_start_url)
                setText("http://www.columbia.edu/~fdc/sample.html")
                maxLines = 1
                inputType = InputType.TYPE_CLASS_TEXT
            }.lparams(matchConstraint, wrapContent)

            searchTextField = editText {
                id = SEARCH_TEXT_EDIT_ID
                hint = context.getString(R.string.search_text)
                setText("commands")
                maxLines = 1
                inputType = InputType.TYPE_CLASS_TEXT
            }.lparams(matchConstraint, wrapContent)

            threadsField = editText {
                id = SEARCH_THREADS_EDIT_ID
                hint = context.getString(R.string.search_threads)
                setText("4")
                maxLines = 1
                inputType = InputType.TYPE_CLASS_NUMBER
                nextFocusDownId = SEARCH_LIMIT_EDIT_ID
            }.lparams(matchConstraint, wrapContent)

            searchLimitField = editText {
                id = SEARCH_LIMIT_EDIT_ID
                hint = context.getString(R.string.search_limit)
                setText("200")
                maxLines = 1
                inputType = InputType.TYPE_CLASS_NUMBER
            }.lparams(matchConstraint, wrapContent)

            recyclerView {
                layoutManager = LinearLayoutManager(context)
                adapter = searchAdapter
                id = RECYCLER_ID
            }.lparams(matchConstraint, matchConstraint)

            applyConstraints()
        }
    }

    private fun _ConstraintLayout.applyConstraints() {
        applyConstraintSet {
            PROGRESS_ID {
                connect(
                    BOTTOM to BOTTOM of PARENT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }

            START_BUTTON_ID {
                connect(
                    BOTTOM to TOP of PROGRESS_ID,
                    START to START of PARENT_ID,
                    END to START of STOP_BUTTON_ID
                )
            }

            STOP_BUTTON_ID {
                connect(
                    BOTTOM to TOP of PROGRESS_ID,
                    START to END of START_BUTTON_ID,
                    END to START of PAUSE_SWITCH_ID
                )
            }

            PAUSE_SWITCH_ID {
                connect(
                    BOTTOM to BOTTOM of STOP_BUTTON_ID,
                    TOP to TOP of STOP_BUTTON_ID,
                    START to END of STOP_BUTTON_ID,
                    END to END of PARENT_ID
                )
            }

            SEARCH_THREADS_EDIT_ID {
                connect(
                    BOTTOM to TOP of START_BUTTON_ID,
                    START to START of PARENT_ID,
                    END to START of SEARCH_LIMIT_EDIT_ID
                )
            }

            SEARCH_LIMIT_EDIT_ID {
                connect(
                    BOTTOM to TOP of START_BUTTON_ID,
                    START to END of SEARCH_THREADS_EDIT_ID,
                    END to END of PARENT_ID
                )
            }

            SEARCH_TEXT_EDIT_ID {
                connect(
                    BOTTOM to TOP of SEARCH_THREADS_EDIT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }

            SEARCH_START_URL_EDIT_ID {
                connect(
                    BOTTOM to TOP of SEARCH_TEXT_EDIT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }

            RECYCLER_ID {
                connect(
                    TOP to TOP of PARENT_ID,
                    BOTTOM to TOP of SEARCH_START_URL_EDIT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }
        }
    }

    fun updateProgress(progress: Int) {
        if (progressBar.progress != progress)
            progressBar.progress = progress
    }

    fun updatePauseSwitch(paused: Boolean) {
        pauseSwitch.isChecked = paused
    }

    fun getSearchInput() = SearchInput(
        startUrlField.text.toString(),
        searchTextField.text.toString(),
        threadsField.text.run { if (isNotEmpty()) toString().toInt() else 0 },
        searchLimitField.text.run { if (isNotEmpty()) toString().toInt() else 0 }
    )
}