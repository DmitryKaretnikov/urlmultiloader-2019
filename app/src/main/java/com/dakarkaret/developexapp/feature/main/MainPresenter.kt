package com.dakarkaret.developexapp.feature.main

import com.dakarkaret.developexapp.feature.BasePresenter
import com.dakarkaret.developexapp.repo.remote.UrlRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainPresenter(private val urlRepo: UrlRepo) :
    BasePresenter<MainView, MainViewState>() {

    override fun bindIntents() {

        intent(MainView::pauseIntent)
            .observeOn(Schedulers.computation())
            .subscribe {
                if (it) {
                    urlRepo.pause()
                } else {
                    urlRepo.continuee()
                }
            }.disposeOnDestroy()

        intent(MainView::stopIntent)
            .observeOn(Schedulers.computation())
            .subscribe { urlRepo.stop() }
            .disposeOnDestroy()

        val updateList = urlRepo.searchUrlsObservable().map { UpdateList(it) }

        val updateProcess = urlRepo.progressObservable().map { UpdateProcess(it) }

        val paused = urlRepo.pausedObservable().map { UpdatePaused(it) }

        val errorDialog: Observable<MainViewState> = Observable.merge(
            intent(MainView::searchExceptionIntent),
            intent(MainView::startIntent)
                .observeOn(Schedulers.computation())
                .switchMap {
                    if (it.isCorrect()) {
                        urlRepo.start(it)
                        Observable.never()
                    } else {
                        Observable.just(IllegalStateException(INVALID_INPUT_MESSAGE))
                    }
                })
            .map { ErrorDialog(it) }

        val hideDialog = intent(MainView::hideDialogIntent).map { HideDialog() }

        subscribeViewState(
            Observable.merge(listOf(updateProcess, updateList, errorDialog, hideDialog, paused))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { Error(it) }
                .scan(Empty as MainViewState) { previousState, changedStatePart ->
                    changedStatePart.reduce(previousState)
                }, MainView::render
        )
    }

    companion object {
        private const val INVALID_INPUT_MESSAGE =
            "Please enter valid url and not empty text. Numbers of threads and searches limit must be more than 1, too"
    }
}