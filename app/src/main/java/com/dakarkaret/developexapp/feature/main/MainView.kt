package com.dakarkaret.developexapp.feature.main

import com.dakarkaret.developexapp.entity.SearchInput
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface MainView : MvpView {

    fun render(viewState: MainViewState)

    fun startIntent(): Observable<SearchInput>

    fun stopIntent(): Observable<Unit>

    fun pauseIntent(): Observable<Boolean>

    fun searchExceptionIntent(): Observable<Throwable>

    fun hideDialogIntent(): Observable<Unit>
}