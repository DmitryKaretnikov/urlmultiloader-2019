package com.dakarkaret.developexapp.feature.main

import com.dakarkaret.developexapp.entity.UrlNode

sealed class MainViewState {

    var progress: Int = 0
        protected set
    var urls: List<UrlNode> = emptyList()
        protected set
    var paused = false
        protected set

    open fun reduce(previousState: MainViewState) = copy(previousState)

    fun copy(
        previousState: MainViewState,
        progress: Int = previousState.progress,
        urls: List<UrlNode> = previousState.urls,
        paused: Boolean = previousState.paused
    ): MainViewState {
        this.progress = progress
        this.urls = urls
        this.paused = paused
        return this
    }

    override fun toString(): String {
        return """
            ${this.javaClass.simpleName}
            progress: $progress
            urls: $urls
            paused: $paused
        """.trimIndent()
    }
}

class UpdateProcess(private val newProgress: Int) : MainViewState() {

    override fun reduce(previousState: MainViewState) = copy(previousState, progress = newProgress)
}

class UpdateList(private val newUrls: List<UrlNode>) : MainViewState() {

    override fun reduce(previousState: MainViewState) = copy(previousState, urls = newUrls)
}

class ErrorDialog(val error: Throwable) : MainViewState()

class HideDialog : MainViewState()

class UpdatePaused(private val newPaused: Boolean) : MainViewState() {

    override fun reduce(previousState: MainViewState) = copy(previousState, paused = newPaused)
}

class Error(val error: Throwable) : MainViewState()

object Empty : MainViewState()