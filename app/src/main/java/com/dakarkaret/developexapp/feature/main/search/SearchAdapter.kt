package com.dakarkaret.developexapp.feature.main.search

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dakarkaret.developexapp.entity.UrlNode
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.AnkoContext

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private val searchThrowableSubject = PublishSubject.create<Throwable>().toSerialized()
    var searches: List<UrlNode> = listOf()
        set(value) {
            val diffResult = DiffUtil.calculateDiff(SearchDiff(field, value))
            field = value
            diffResult.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SearchLayout().let { SearchViewHolder(it, it.createView(AnkoContext.create(parent.context, parent))) }

    override fun getItemCount() = searches.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(searches[position])
    }

    fun getSearchThrowableObservable(): Observable<Throwable> = searchThrowableSubject

    inner class SearchViewHolder(private val searchLayout: SearchLayout, searchView: View) :
        RecyclerView.ViewHolder(searchView) {

        fun bind(urlNode: UrlNode) {
            with(searchLayout.urlTxt) {
                text = urlNode.url
                setTextColor(ContextCompat.getColor(itemView.context, urlNode.status.color))
                urlNode.error?.let { error -> setOnClickListener { searchThrowableSubject.onNext(error) } }
            }
        }
    }
}
