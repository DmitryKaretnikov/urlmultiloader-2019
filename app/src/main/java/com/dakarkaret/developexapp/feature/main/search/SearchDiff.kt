package com.dakarkaret.developexapp.feature.main.search

import androidx.recyclerview.widget.DiffUtil
import com.dakarkaret.developexapp.entity.UrlNode

class SearchDiff(private val oldList: List<UrlNode>, private val newList: List<UrlNode>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].url == newList[newItemPosition].url

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].status == newList[newItemPosition].status &&
                oldList[oldItemPosition].error == newList[newItemPosition].error
}