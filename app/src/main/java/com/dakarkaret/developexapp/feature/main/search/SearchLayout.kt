package com.dakarkaret.developexapp.feature.main.search

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import com.dakarkaret.developexapp.R
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.*
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint


class SearchLayout : AnkoComponent<ViewGroup> {

    lateinit var urlTxt: TextView
        private set

    companion object {
        const val URL_ID = 1101
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        constraintLayout {

            lparams(matchParent, wrapContent)

            urlTxt = textView {
                id = URL_ID
            }.lparams(matchConstraint, wrapContent)

            applyConstraintSet {
                URL_ID {
                    connect(
                        TOP to TOP of PARENT_ID margin dimen(R.dimen.default_margin),
                        BOTTOM to BOTTOM of PARENT_ID margin dimen(R.dimen.default_margin),
                        START to START of PARENT_ID margin dimen(R.dimen.default_margin),
                        END to END of PARENT_ID margin dimen(R.dimen.default_margin)
                    )
                }
            }
        }
    }
}