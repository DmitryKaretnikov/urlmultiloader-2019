package com.dakarkaret.developexapp.repo.html

interface HtmlRepo {

    fun loadHtml(url: String): String
}