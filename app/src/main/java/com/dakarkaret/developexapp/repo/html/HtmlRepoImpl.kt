package com.dakarkaret.developexapp.repo.html

import com.github.kittinunf.fuel.httpGet

class HtmlRepoImpl : HtmlRepo {

    override fun loadHtml(url: String) = url.httpGet()
        .responseString()
        .let { (_, response, result) ->
            result.get().let {
                if (it.lines().first() == "<!DOCTYPE html>" || response.headers["Content-Type"].contains("text/html"))
                    it
                else
                    throw NoSuchElementException("Sorry, I'm not html")
            }
        }
}