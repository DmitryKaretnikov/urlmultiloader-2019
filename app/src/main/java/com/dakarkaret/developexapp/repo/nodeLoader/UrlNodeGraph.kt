package com.dakarkaret.developexapp.repo.nodeLoader

import com.dakarkaret.developexapp.entity.UrlNode

interface UrlNodeGraph {

    fun setup(startUrl: String, searchLimit: Int, searchText: String)

    fun loadNode(node: UrlNode)

    fun getUnrefinedNode(): UrlNode?

    fun graphAsList(): List<UrlNode>
}