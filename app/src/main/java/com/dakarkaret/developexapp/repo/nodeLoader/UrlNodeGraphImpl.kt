package com.dakarkaret.developexapp.repo.nodeLoader

import com.dakarkaret.developexapp.entity.UrlNode
import com.dakarkaret.developexapp.entity.UrlStatus
import com.dakarkaret.developexapp.entity.UrlStatus.ERROR
import com.dakarkaret.developexapp.repo.html.HtmlRepo

class UrlNodeGraphImpl(private val htmlRepo: HtmlRepo) : UrlNodeGraph {

    private val webAddressRegex = Regex(WEB_ADDRESS_PATTERN)
    private var searchLimit = 0
    private var searchText = ""
    private lateinit var startNode: UrlNode

    override fun setup(startUrl: String, searchLimit: Int, searchText: String) {
        this.searchText = searchText
        this.searchLimit = searchLimit
        startNode = UrlNode(startUrl)
    }

    override fun loadNode(node: UrlNode) {
        try {
            val html = htmlRepo.loadHtml(node.url)
            addChildren(node, html)
            node.status = if (html.contains(searchText, ignoreCase = true)) UrlStatus.SUCCESS else UrlStatus.EMPTY
        } catch (e: Exception) {
            node.status = ERROR
            node.error = e
        }
    }

    private fun addChildren(parent: UrlNode, html: String) {
        if (searchLimit > 0) {
            webAddressRegex.findAll(html)
                .map { it.value }
                .distinct()
                .forEach {
                    if (searchLimit > 0) {
                        parent.addChild(UrlNode(it))
                        searchLimit--
                    }
                }
        }
    }

    override fun graphAsList(): List<UrlNode> = mutableListOf(startNode).apply {
        var i = 0
        while (i < size) {
            get(i)?.let { addAll(it.children) }
            i++
        }
    }

    @Synchronized
    override fun getUnrefinedNode() = try {
        graphAsList().first { it.status == UrlStatus.WAIT }
            .apply { status = UrlStatus.LOADING }
    } catch (e: NoSuchElementException) {
        null
    }

    companion object {
        private const val WEB_ADDRESS_PATTERN =
            "https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\\.[a-z]{2,4}\\b([-a-zA-Z0-9@:%_+.~#?&/=]*)"
    }
}