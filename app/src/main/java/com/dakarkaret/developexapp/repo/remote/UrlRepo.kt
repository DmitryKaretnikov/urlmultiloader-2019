package com.dakarkaret.developexapp.repo.remote

import com.dakarkaret.developexapp.entity.SearchInput
import com.dakarkaret.developexapp.entity.UrlNode
import io.reactivex.Observable

interface UrlRepo {

    fun searchUrlsObservable(): Observable<List<UrlNode>>

    fun progressObservable(): Observable<Int>

    fun pausedObservable(): Observable<Boolean>

    fun start(searchInput: SearchInput)

    fun pause()

    fun continuee()

    fun stop()
}