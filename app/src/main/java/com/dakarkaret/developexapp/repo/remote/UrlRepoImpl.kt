package com.dakarkaret.developexapp.repo.remote

import android.util.Log
import com.dakarkaret.developexapp.entity.SearchInput
import com.dakarkaret.developexapp.entity.UrlNode
import com.dakarkaret.developexapp.entity.UrlStatus
import com.dakarkaret.developexapp.repo.nodeLoader.UrlNodeGraph
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class UrlRepoImpl(private val urlNodeGraph: UrlNodeGraph) : UrlRepo {

    private val searchUrlsSubject = BehaviorSubject.createDefault(emptyList<UrlNode>())
    private val progressSubject = BehaviorSubject.createDefault(0)
    private val pausedSubject = BehaviorSubject.createDefault(false)
    private val pleaseCheck = PublishSubject.create<Unit>()

    private var disposable: Disposable? = null
    private var searchLimit: Int = 1
    private var threadPool: ThreadPoolExecutor? = null

    override fun start(searchInput: SearchInput) {
        stop()

        searchLimit = searchInput.searchLimit
        urlNodeGraph.setup(searchInput.startUrl, searchLimit, searchInput.searchText)

        threadPool = Executors.newFixedThreadPool(searchInput.threads) as ThreadPoolExecutor

        disposable = pausedSubject
            .switchMap {
                if (it) {
                    Observable.never()
                } else {
                    pleaseCheck
                        .flatMap { Observable.range(0, searchInput.threads) }
                        .flatMap { Observable.fromCallable { loadNode() }.subscribeOn(Schedulers.from(threadPool!!)) }
                }
            }
            .subscribe ({},{ Log.e("UrlRepoImpl", "loop error:", it)})

        pleaseCheck.onNext(Unit)
    }

    private fun loadNode() {
        urlNodeGraph.getUnrefinedNode()?.let {
            updateOutput()
            urlNodeGraph.loadNode(it)
            updateOutput()
            pleaseCheck.onNext(Unit)
        }
    }

    private fun updateOutput() {
        val list = urlNodeGraph.graphAsList().map { it.copy() }
        searchUrlsSubject.onNext(list)

        val progressInPct =
            list.count { it.status != UrlStatus.WAIT && it.status != UrlStatus.LOADING }.toFloat() / (searchLimit + 1) * 100
        progressSubject.onNext(progressInPct.toInt())
    }

    override fun stop() {
        disposable?.dispose()
        threadPool?.shutdown()
    }

    override fun pause() {
        pausedSubject.onNext(true)
    }

    override fun continuee() {
        pausedSubject.onNext(false)
        pleaseCheck.onNext(Unit)
    }

    override fun searchUrlsObservable(): Observable<List<UrlNode>> =
        searchUrlsSubject.throttleLatest(1, TimeUnit.SECONDS)

    override fun progressObservable(): Observable<Int> = progressSubject.throttleLatest(1, TimeUnit.SECONDS)

    override fun pausedObservable() = pausedSubject
}