package com.dakarkaret.developexapp

import com.dakarkaret.developexapp.entity.SearchInput
import com.dakarkaret.developexapp.feature.main.MainPresenter
import com.dakarkaret.developexapp.feature.main.MainView
import com.dakarkaret.developexapp.feature.main.MainViewState
import com.dakarkaret.developexapp.repo.html.HtmlRepo
import com.dakarkaret.developexapp.repo.nodeLoader.UrlNodeGraphImpl
import com.dakarkaret.developexapp.repo.remote.UrlRepoImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test


class MainPresenterTests {

    companion object {
        private const val TEST_URL = "http://test.url"
        private const val SEARCHED_TEXT = "searched text"
        private const val TEST_HTML = "$SEARCHED_TEXT http://www.thisissite.com http://www.iamsitetoo.com"
    }

    private val mainViewMock = mock<MainView>()
    private val htmlRepoMock = mock<HtmlRepo>()
    private val mainPresenter = MainPresenter(UrlRepoImpl(UrlNodeGraphImpl(htmlRepoMock)))

    private val startIntentSubject = PublishSubject.create<SearchInput>()

    init {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    /**
     * Mock [mainViewMock]'s intents and bind [mainViewMock] to [mainPresenter]
     */
    @Before
    fun before() {
        whenever(mainViewMock.startIntent()).thenReturn(startIntentSubject)
        whenever(mainViewMock.stopIntent()).thenReturn(Observable.never())
        whenever(mainViewMock.pauseIntent()).thenReturn(Observable.just(false))
        whenever(mainViewMock.searchExceptionIntent()).thenReturn(Observable.never())
        whenever(mainViewMock.hideDialogIntent()).thenReturn(Observable.never())

        mainPresenter.attachView(mainViewMock)
    }

    /**
     * After emit a [SearchInput] from [MainView.startIntent],
     * class should render list with 3 Url ([TEST_URL] and 2 Urls from [TEST_HTML])
     */
    @Test
    fun searchUrlsTest() {
        whenever(htmlRepoMock.loadHtml(TEST_URL)).thenReturn(TEST_HTML)

        startIntentSubject.onNext(SearchInput(TEST_URL, SEARCHED_TEXT, 5, 20))

        Thread.sleep(5000)

        argumentCaptor<MainViewState>().apply {
            verify(mainViewMock, times(6)).render(capture())
            assertEquals(
                " Expected: 3, found: ${lastValue.urls.map { it.url }}",
                3, lastValue.urls.size
            )
        }
    }

    /**
     * Close Presenter's Disposables
     */
    @After
    fun after() {
        mainPresenter.destroy()
    }
}